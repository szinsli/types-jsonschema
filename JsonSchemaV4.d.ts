//TODO look at the spec properly
export interface JsonSchema {
    title?: string;
    enum?: any[];
    type?: string|string[];
    readonly?: boolean;
    description?: string;
    properties?: JsonSchemaProperties;
    required?: string[];
    items?: JsonSchema;
    maximum?: number;
    minimum?: number;
    minLength?: number;
    maxLength?: number;
    format?: string;
    allOf?: JsonSchema[];
    anyOf?: JsonSchema[];
    oneOf?: JsonSchema[];
    not?: JsonSchema;
    definitions?: JsonSchemaProperties;
    default?: any;
}

export interface JsonSchemaProperties {
    [key: string]: JsonSchema;
}
